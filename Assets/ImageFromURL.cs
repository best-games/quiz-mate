using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ImageFromURL : MonoBehaviour {

	string address;

	private void OnEnable() {
		SetImage();
	}

	public void SetImage() {
		address = CurrentData.imageURL[Values.currentQuestion];
		StartCoroutine(DownloadImage());
	}


	IEnumerator DownloadImage() {
		WWW www = new WWW(address);
		while (!www.isDone)
			yield return null;
		GetComponent<RawImage>().texture = www.texture;
	}
}