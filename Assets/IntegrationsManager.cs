using UnityEngine;
using AppsFlyerSDK;
using Firebase.Analytics;
using GameAnalyticsSDK;
using RTBHouse;
using System.Collections.Generic;

public class IntegrationsManager : MonoBehaviour {

	private static IntegrationsManager _instance; 
	
	bool firebaseActive = false;
	private static RtbHouseSdk rtbHouseSdk;

	void Awake() {
		if (_instance == null) {
			_instance = this;
			DontDestroyOnLoad(this.gameObject);
		}
		else {
			Destroy(this);
		}
	}

	void Start() {
		AppsFlyer.sendEvent("af_login", null);
		
		GameAnalytics.Initialize();
		Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(
			task =>
			{
				var dependencyStatus = task.Result;
				if (dependencyStatus == Firebase.DependencyStatus.Available) {
					firebaseActive = true;
				}
				else {
					UnityEngine.Debug.LogError(System.String.Format("Could not resolve all Firebase dependencies: {0}", dependencyStatus));
				}
			});

		rtbHouseSdk = gameObject.AddComponent<RtbHouseSdk>();
		rtbHouseSdk.Init("c5b19d536eadb96c329728ea318aef31", "U7Fx3sH7u8htx4pGcxxu", "us");
		rtbHouseSdk.HandleCustomCodeTag("IS-app-open", "1");
	}

	public void CategoryEntered(string name)
	{
		rtbHouseSdk.HandleCustomCodeTag("IS-quiz-open", name);
		
		Dictionary<string, string> QuizOpen = new Dictionary<string, string>();
		QuizOpen.Add("category-name", name);
		AppsFlyer.sendEvent ("af_quiz_open", QuizOpen);
	}

	public void CategoryExited(string categoryName,float timeSpent, int questionsShowed, float winRate) {
		rtbHouseSdk.HandleCustomCodeTag("IS-quiz-exit", categoryName);
		rtbHouseSdk.HandleCustomCodeTag("IS-quiz-time-spent", ""+timeSpent);
		rtbHouseSdk.HandleCustomCodeTag("IS-quiz-questions", ""+questionsShowed);
		rtbHouseSdk.HandleCustomCodeTag("IS-quiz-winrate", ""+winRate);
		
		Dictionary<string, string> QuizExit = new Dictionary<string, string>();
		QuizExit.Add("quiz-exit", categoryName);
		QuizExit.Add("quiz-time-spent", timeSpent.ToString());
		QuizExit.Add("quiz-questions", questionsShowed.ToString());
		QuizExit.Add("quiz-winrate", winRate.ToString());
		AppsFlyer.sendEvent ("af_quiz_exit", QuizExit);
	}
}
