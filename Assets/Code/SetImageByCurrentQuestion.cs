using UnityEngine;
using UnityEngine.UI;


public class SetImageByCurrentQuestion : MonoBehaviour {

	public Sprite[] imagesMinecraft, imagesUsedCars, imagesFriends;

	public void SetImage() {
		if (Values.currentCategory == "Minecraft") {
			GetComponent<Image>().sprite = imagesMinecraft[Values.currentQuestion];
		}
		else if (Values.currentCategory == "UsedCars") {
			GetComponent<Image>().sprite = imagesUsedCars[Values.currentQuestion];
		}
		else if (Values.currentCategory == "Friends") {
			GetComponent<Image>().sprite = imagesFriends[Values.currentQuestion];
		}

	}

	void Update() {
		
	}
}
