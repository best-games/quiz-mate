﻿public static class Values {

	public static string currentCategory = "Minecraft";
	public static int currentQuestion = 0;
	public static int correctAnswers = 0;
	public static int AcceptedGDPR = 0;
	
	public static void ResetValues(){
		currentQuestion = 0;
		correctAnswers = 0;
	}
}
