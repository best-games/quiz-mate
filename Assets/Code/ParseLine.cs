using System;
using System.Collections.Generic;
using System.Linq;

public static class ParseLine {

	public static string[] Parse(string line) {
		List<string> list = new List<string>();

		while (true) {
			if (String.IsNullOrEmpty(line)) break;
			bool isQuoted = (line[0] == '\"');

			if (!isQuoted) {
				int idx = line.IndexOf(',');
				if (idx == -1) {
					list.Add(line);
					break;
				}

				var field = line.Substring(0, idx);
				list.Add(field);
				line = line.Substring(idx + 1);
				continue;
			}
			else {
				int candidateIndex = -1;

				while (true) {
					int start = 1;

					candidateIndex = line.IndexOf("\",", start);
					if (candidateIndex == -1) {
						line = line.TrimEnd('\"');
						break;
					}

					int cnt = line.Substring(1, candidateIndex - 1).Where(c => c == '\"').Count();
					if ((cnt % 2) == 0) break;

					start = candidateIndex + 2;
				}

				if (candidateIndex == -1) {
					list.Add(line.Replace("\"\"", "\""));
					break;
				}

				var field = line.Substring(1, candidateIndex - 1);
				list.Add(field.Replace("\"\"", "\""));

				line = line.Substring(candidateIndex + 2);
			}
		}

		return list.ToArray();
	}
}