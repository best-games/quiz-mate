using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

	public Text headerText;
	public Text questionText;
	public Transform imageParent;
	public RawImage image;
	public Slider timeBar;
	public Image timeBarImage;
	public GameObject pausePanel;
	public float startDelayMax = 4f;
	public float timeToAnswer = 10f;
	public int gameState = 0; // 0 Start, 1 Waiting for answer, 2 Answered, 3 Time's up
	public Button[] buttons;
	public Text[] buttonTexts;
	public Sprite defaultButton;
	public Sprite pressedButton;
	public Sprite correctButton;
	public Sprite wrongButton;
	AudioSource sound;
	public IntegrationsManager im;
	public float timeSpent = 0f;

	List<int> categoryCorrectAnswers = new List<int>();
	int currentButtonId;
	float startDelay = 4f;
	int questionsCount = 10;

	void OnEnable() {
		sound = GetComponent<AudioSource>();
		timeBar.maxValue = timeToAnswer;
		im = FindObjectOfType<IntegrationsManager>();
		InitializeElements();
	}

	void InitializeElements() {
		headerText.text = Values.currentCategory+": "+(Values.currentQuestion+1)+"/"+ questionsCount;
		startDelay = startDelayMax;
		gameState = 0;
		questionText.transform.localScale = new Vector3(1f, 1f, 1f);
		imageParent.localScale = new Vector3(1f, 1f, 1f);
		timeBar.value = timeBar.maxValue;
		timeBarImage.color = new Color(.12f, .42f, .82f);
		timeBar.transform.localScale = new Vector3(1f, 1f, 1f);

		// Get questions 

		categoryCorrectAnswers = CurrentData.correctAnswer;
		questionText.text = CurrentData.questions[Values.currentQuestion];
		buttonTexts[0].text = CurrentData.answer1[Values.currentQuestion];
		buttonTexts[1].text = CurrentData.answer2[Values.currentQuestion];
		buttonTexts[2].text = CurrentData.answer3[Values.currentQuestion];
		buttonTexts[3].text = CurrentData.answer4[Values.currentQuestion];


		foreach (Button b in buttons) {
			b.interactable = true;
		}


		questionText.transform.localScale = new Vector3(0, 0, 0);
		questionText.transform.DOScale(new Vector3(1,1,1), 1f);
		image.GetComponent<ImageFromURL>().SetImage();
		imageParent.position = new Vector3(-7, image.transform.position.y, image.transform.position.z);
		imageParent.DOLocalMoveX(0, 1f).SetEase(Ease.OutCubic).SetDelay(0.5f);

		// Reset buttons
		float animSpeed = .5f;
		buttons[0].GetComponent<AnswerButton>().ResetAnswer();
		buttons[1].GetComponent<AnswerButton>().ResetAnswer();
		buttons[2].GetComponent<AnswerButton>().ResetAnswer();
		buttons[3].GetComponent<AnswerButton>().ResetAnswer();
		buttons[0].transform.localScale = new Vector3(0, 0, 0);
		buttons[1].transform.localScale = new Vector3(0, 0, 0);
		buttons[2].transform.localScale = new Vector3(0, 0, 0);
		buttons[3].transform.localScale = new Vector3(0, 0, 0);
		buttons[0].transform.DOScale(new Vector3(1, 1, 1), animSpeed);
		buttons[1].transform.DOScale(new Vector3(1, 1, 1), animSpeed).SetDelay(animSpeed);
		buttons[2].transform.DOScale(new Vector3(1, 1, 1), animSpeed).SetDelay(animSpeed * 2);
		buttons[3].transform.DOScale(new Vector3(1, 1, 1), animSpeed).SetDelay(animSpeed * 3);
	}

	void FixedUpdate() {

		timeSpent += Time.fixedDeltaTime;

		if (gameState == 0) {
			startDelay -= Time.fixedDeltaTime;
			if (startDelay <= 0) {
				gameState = 1;
			}
		}

		if (gameState == 1) {
			timeBar.value -= Time.fixedDeltaTime;
			if (timeBar.value <= 3) {
				timeBarImage.DOColor(new Color(.8f, .1f, .2f), .6f);
			}
			if (timeBar.value <= 0) {
				foreach (Button b in buttons) {
					b.interactable = false;
					sound.Play();
				}
				Invoke("ShowCorrectAnswer", 1f);
				Invoke("NextQuestion", 2.5f);
				gameState = 3;
			}
		}
	}

	public void Pause() {
		Time.timeScale = 0;
		pausePanel.SetActive(true);
	}

	public void Unpause() {
		Time.timeScale = 1;
		pausePanel.SetActive(false);
	}

	public void AnswerPressed(int buttonId) {
		gameState = 2;
		currentButtonId = buttonId;
		buttons[currentButtonId].image.sprite = pressedButton;
		
		foreach(Button b in buttons) {
			b.interactable = false;
		}

		Invoke("VerifyAnswer", 0.4f);
	}

	public void VerifyAnswer() {
		if (currentButtonId == CurrentData.correctAnswer[Values.currentQuestion]) {
			Values.correctAnswers++;
			ShowCorrectAnswer();
			Invoke("HideElements", 1f);
			Invoke("NextQuestion", 1.5f);
		}
		else {
			ShowWrongAnswer();
			Invoke("ShowCorrectAnswer", 1f);
			Invoke("HideElements", 2.5f);
			Invoke("NextQuestion", 3f);
		}

		print(Values.currentQuestion);
	}

	void ShowCorrectAnswer() {
		for (int i = 0; i < buttons.Length; i++) {
			if (i == CurrentData.correctAnswer[Values.currentQuestion]) {
				buttons[i].GetComponent<AnswerButton>().CorrectAnswer();
			}
		}
	}

	void ShowWrongAnswer() {
		for (int i = 0; i < buttons.Length; i++) {
			if (i == currentButtonId) {
				buttons[i].GetComponent<AnswerButton>().WrongAnswer();
			}
		}
	}

	void HideElements() {
		float hideSpeed = .4f;
		imageParent.transform.DOScaleY(0, hideSpeed);
		timeBar.transform.DOScaleY(0, hideSpeed);
		questionText.transform.DOScaleY(0, hideSpeed);
		foreach (Button b in buttons) {
			b.transform.DOScaleY(0, hideSpeed);
		}
	}

	void NextQuestion() {
		Values.currentQuestion++;
		if (Values.currentQuestion >= questionsCount) {
			float winRate = (float)Values.correctAnswers/questionsCount;
			im.CategoryExited(Values.currentCategory, timeSpent, Values.currentQuestion, winRate);
			SceneManager.LoadScene("Summary");
		}
		else {
			InitializeElements();
		}
	}

	public void ReturnToMenu() {
		Time.timeScale = 1;
		SceneManager.LoadScene("MainMenu");
		float winRate = (float)Values.correctAnswers/questionsCount;
		print("winrate: "+winRate);
		im.CategoryExited(Values.currentCategory, timeSpent, Values.currentQuestion, winRate);
	}
}
