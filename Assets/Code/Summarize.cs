using UnityEngine;
using UnityEngine.UI;

public class Summarize : MonoBehaviour {

	public Text categoryText;
	public Text answersText;
	public Text scoreCommentText;

	void OnEnable() {
		categoryText.text = "Category: " + Values.currentCategory;
		answersText.text = "Correct answers: " + Values.correctAnswers + " from 10.";

		if (Values.correctAnswers <= 1) {
			scoreCommentText.text = "Try again and get better!";
		}
	}

	void Update() {
		
	}
}
