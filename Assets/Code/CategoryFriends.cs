﻿public static class CategoryFriends {

	public static string[] questions = new string[]{
		"What is the name of Rachel's father?",
		"What is Chandler's profession?",
		"In the twenty-first episode of Season 1 Ross was forced to give Marcel away, where did he put him?",
		"What is the name of the first episode of \"Friends\"?"

	};

	public static string[] answer1 = new string[]{
		"Dr.Leonard Green",
		"Business Analyst",
		"South America",
		"The One with the Fake Monica"
	};	
	public static string[] answer2 = new string[]{
		"Dr.Peter Green",
		"Statistical Analysis",
		"Circus",
		"The One with the Sonogram at the End"
	};	
	public static string[] answer3 = new string[]{
		"Dr.James Green",
		"Programmer",
		"San Diego Zoo",
		"The One Where Monica Gets a Roommate"
	};	
	public static string[] answer4 = new string[]{
		"Dr.Christopher Green",
		"Business consultant",
		"National Zoological Park",
		"The One with the Birth"
	};
	public static int[] correctAnswer = new int[]{
		0,1,2,2
	};

}
