using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;

public class AnswerButton : MonoBehaviour, IPointerDownHandler {

	public Sprite basicSprite;
	public Image correctImage;
	public Image wrongImage;
	float fadeSpeed = .5f;
	AudioSource sound;
	[Header("Sounds")]
	public AudioClip ButtonDownSound;
	public AudioClip CorrectSound;
	public AudioClip WrongSound;

	private void OnEnable() {
		sound = GetComponent<AudioSource>();
	}

	public void OnPointerDown(PointerEventData eventData) {
		sound.clip = ButtonDownSound;
		sound.Play();
	}

	public void ResetAnswer() {
		correctImage.DOFade(0f, 0f);
		wrongImage.DOFade(0f, 0f);
		GetComponent<Image>().sprite = basicSprite;
	}

	public void CorrectAnswer() {
		correctImage.DOFade(1f, fadeSpeed);
		sound.clip = CorrectSound;
		sound.Play();
	}

	public void WrongAnswer() {
		wrongImage.DOFade(1f, fadeSpeed);
		sound.clip = WrongSound;
		sound.Play();
	}
}
