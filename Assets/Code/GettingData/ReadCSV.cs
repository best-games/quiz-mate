using UnityEngine;
using System.IO;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class ReadCSV : MonoBehaviour {

	public GameObject button;
	public GameObject buttonsGroup;
	public CategorySelection categorySelection;

	void OnEnable() {
		StartCoroutine(DownloadCSV());
		
	}

	IEnumerator DownloadCSV() {
		UnityWebRequest w = UnityWebRequest.Get("https://storage.googleapis.com/quiz_game_feed/Quizz%20Feed%20Template%20-%20Feed.csv");
		yield return w.SendWebRequest();

		if (w.result == UnityWebRequest.Result.ConnectionError || w.result == UnityWebRequest.Result.ProtocolError) {
			print(w.error);
		}
		else {
			string savePath = string.Format("{0}/{1}.csv", Application.persistentDataPath, "QuizData");
			File.WriteAllText(savePath, w.downloadHandler.text);
			ReadCSVFile();
		}
	}
	
	void ReadCSVFile() {
		StreamReader strReader = new StreamReader(Application.persistentDataPath+ "//QuizData.csv");
		bool endOfFile = false;

		while (!endOfFile) {
			string dataString = strReader.ReadLine();
			if(dataString == null) {
				endOfFile = true;
				break;
			}

			//var dataValues = dataString.Split(',');
			//var dataValues = Regex.Split(dataString, ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
			//dataValues.ForEach(i => i.Trim('\"'));
			var dataValues = ParseLine.Parse(dataString);
			

			if (dataValues[1].ToString() != "category" && !CurrentData.categories.Contains(dataValues[1].ToString())) {
				CurrentData.categories.Add(dataValues[1].ToString());
			}
		}

		for (int i = 0; i < CurrentData.categories.Count; i++) {
			var a = i;
			var newButton = Instantiate(button, buttonsGroup.transform);
			newButton.name = "Button " + CurrentData.categories[i];
			newButton.GetComponentInChildren<Text>().text = CurrentData.categories[i];
			newButton.GetComponent<Button>().onClick.AddListener(() => categorySelection.SetCategory(CurrentData.categories[a]));
		}
	}
}
