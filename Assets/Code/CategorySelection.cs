using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class CategorySelection : MonoBehaviour {

	public FadeImage fadeImage;
	public IntegrationsManager im;
	AudioSource sound;

	private void OnEnable() {
		sound = GetComponent<AudioSource>();
		im = FindObjectOfType<IntegrationsManager>();
		if (fadeImage) {
			fadeImage.gameObject.SetActive(true);
		}
	}

	public void SetCategory(string categoryName) {
		sound.Play();
		Values.correctAnswers = 0;
		Values.currentQuestion = 0;
		Values.currentCategory = categoryName;
		im.CategoryEntered(categoryName);
		print(Values.currentCategory);

		StreamReader strReader = new StreamReader(Application.persistentDataPath + "//QuizData.csv");
		bool endOfFile = false;

		CurrentData.questions.Clear();
		CurrentData.answer1.Clear();
		CurrentData.answer2.Clear();
		CurrentData.answer3.Clear();
		CurrentData.answer4.Clear();
		CurrentData.imageURL.Clear();
		CurrentData.correctAnswer.Clear();

		while (!endOfFile) {
			string dataString = strReader.ReadLine();
			if (dataString == null) {
				endOfFile = true;
				break;
			}

			//var dataValues = dataString.Split(',');
			//var dataValues = Regex.Split(dataString, ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
			//dataValues.ForEach(i => i.Trim('\"'));

			var dataValues = ParseLine.Parse(dataString);

			if (dataValues[1].ToString() == categoryName) {
				CurrentData.questions.Add(dataValues[3].ToString());
				CurrentData.answer1.Add(dataValues[4].ToString());
				CurrentData.answer2.Add(dataValues[5].ToString());
				CurrentData.answer3.Add(dataValues[6].ToString());
				CurrentData.answer4.Add(dataValues[7].ToString());
				CurrentData.imageURL.Add(dataValues[8].ToString());
				CurrentData.correctAnswer.Add(int.Parse(dataValues[9])-1);
			}
		}

		if (fadeImage) {
			fadeImage.FadeOut();
		}

		Invoke("StartGame", 1f);
	}

	public void StartGame() {
		SceneManager.LoadScene("Gameplay");
	}
}
