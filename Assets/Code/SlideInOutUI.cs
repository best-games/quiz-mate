using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class SlideInOutUI : MonoBehaviour {

	public Vector2 startPosition;
	public Vector2 endPosition;
	
	float slideSpeed = .7f;
	Vector2 basePosition;

	void OnEnable() {
		basePosition = GetComponent<RectTransform>().localPosition;
		
		transform.position += (Vector3)startPosition;
		endPosition = basePosition + endPosition;

		StartSlide();
	}

	public void StartSlide() {
		transform.DOLocalMove(basePosition, slideSpeed);
	}

	public void EndSlide() {
		transform.DOLocalMove(endPosition, slideSpeed);
	}
}
