﻿public static class CategoryUsedCars {

	public static string[] questions = new string[]{
		"Which of these cars received the \"Never Buy\" label?",
		"What was the largest engine fitted to the Chevrolet Camaro(1970–1981)?",
		"When was the production of the car shown in the picture finished?",
		"What is the average price of 2017 Chevrolet Suburban according to cars.usnews.com ?"
	};

	public static string[] answer1 = new string[]{
		"BMW X5 (2012)",
		"6.6L V8",
		"1975",
		"$25,000"
	};	
	public static string[] answer2 = new string[]{
		"Honda HR-V (2021)",
		"7L V8",
		"1976",
		"$35,000"
	};	
	public static string[] answer3 = new string[]{
		"Toyota Prius (2020)",
		"5.7L V8",
		"1978",
		"$45,000"
	};	
	public static string[] answer4 = new string[]{
		"Linkoln MKZ (2017)",
		"7.2L V8",
		"1981",
		"$55,000"
	};
	public static int[] correctAnswer = new int[]{
		0,0,1,3
	};

}
