﻿using System.Collections.Generic;

public static class CurrentData {

	public static List<string> categories = new List<string>();

	public static List<string> questions = new List<string>();
	public static List<string> answer1 = new List<string>();
	public static List<string> answer2 = new List<string>();
	public static List<string> answer3 = new List<string>();
	public static List<string> answer4 = new List<string>();
	public static List<string> imageURL = new List<string>();
	public static List<int> correctAnswer = new List<int>();
}
