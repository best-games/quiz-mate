﻿public static class CategoryMinecraft {

	public static string[] questions = new string[]{
		"What is this block?",
		"How to \"win\" Minecraft?",
		"What do you need to create \"Potion of Healing\"?",
		"Which one of these Ores does not exist in vanilla Minecraft?"
	};

	public static string[] answer1 = new string[]{
		"Crying Obsidian",
		"Build a house",
		"1 Water bottle, 1 Nether wart, 1 Glistering melon",
		"Gold Ore"
	};	
	public static string[] answer2 = new string[]{
		"Dark Prismarine",
		"You can't",
		"1 Nether wart, 1 Water bottle",
		"Emerald Ore"
	};	
	public static string[] answer3 = new string[]{
		"Mossy Cobblestone",
		"Defeat Enderdragon",
		"1 Glowstone dust, 1 Water bottle",
		"Diamond Ore"
	};	
	public static string[] answer4 = new string[]{
		"Sandstone",
		"Gather 1 million blocks",
		"1 Ghast tear, 1 Water bottle",
		"Sapphire Ore"
	};
	public static int[] correctAnswer = new int[]{
		2,2,0,3
	};

}
