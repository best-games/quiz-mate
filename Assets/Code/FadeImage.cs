using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class FadeImage : MonoBehaviour {

	Image image;

	public float fadeInDuration = 1f;
	public float fadeInDelay = 0f;
	public float fadeOutDuration = 1f;

	void OnEnable() {
		image = GetComponent<Image>();
		Invoke("FadeIn", fadeInDelay);
	}

	public void FadeIn() {
		image.DOFade(0f, fadeInDuration);
	}

	public void FadeOut() {
		image.DOFade(1f, fadeOutDuration);
	}
}
