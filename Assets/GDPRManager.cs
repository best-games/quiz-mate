using UnityEngine;

public class GDPRManager : MonoBehaviour {

	public GameObject GDPRPanel;

	void Start() {
		if (PlayerPrefs.HasKey("AcceptedGDPR")) Values.AcceptedGDPR = PlayerPrefs.GetInt("AcceptedGDPR");
		if (Values.AcceptedGDPR == 1) {
			GDPRPanel.SetActive(false);
		}
	}

	public void AcceptGDPR() {
		Values.AcceptedGDPR = 1;
		PlayerPrefs.SetInt("AcceptedGDPR", Values.AcceptedGDPR);
		GDPRPanel.SetActive(false);
	}
}
